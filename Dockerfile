ARG BASE_VERSION=3.14

FROM docker.io/library/alpine:$BASE_VERSION

ENV AWS_CLI_VERSION=1.19.93-r0

ARG BUILD_DATE
ARG VCS_REF
ARG VERSION

VOLUME /data

COPY resources /resources

RUN /resources/build && rm -rf /resources

WORKDIR /data

ENTRYPOINT ["aws"]

LABEL "maintainer"="massiah|->420blaze.it" \
      "org.label-schema.name"="aws-cli" \
      "org.label-schema.base-image.name"="docker.io/library/alpine" \
      "org.label-schema.base-image.version"="$BASE_VERSION" \
      "org.label-schema.description"="AWS-CLI in a container" \
      "org.label-schema.url"="https://aws.amazon.com/cli/" \
      "org.label-schema.vcs-url"="https://gitlab.com/wild-beavers/docker/aws-cli" \
      "org.label-schema.vendor"="WildBeavers" \
      "org.label-schema.schema-version"="1.0.0-rc.1" \
      "org.label-schema.applications.aws-cli.version"="$AWS_CLI_VERSION" \
      "org.label-schema.vcs-ref"=$VCS_REF \
      "org.label-schema.version"=$VERSION \
      "org.label-schema.build-date"=$BUILD_DATE \
      "org.label-schema.usage"="sudo docker run -it help"
